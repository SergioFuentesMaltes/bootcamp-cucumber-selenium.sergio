package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.*;

import javax.print.DocFlavor;
import java.nio.channels.SelectableChannel;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class PonerJugadorVentaSteps {
    private WebDriver driver;
    private ChromeOptions options = new ChromeOptions();


    @Given("^el usuario_ accede a la web de biwenger del login$")
    public void navigate_login_page() throws Exception {
        System.out.println("Go to Login Page");
        System.setProperty("Webdriver.geckodriver", "./src/test/resources/drivers/chromedriver.exe");
        // Muy importante poner Chorme en modo HEADLESS
        options.setHeadless(true);

        driver = new ChromeDriver(options);
        driver.manage().window().maximize();

        // Navegar a la página de Login
        driver.get("https://biwenger.as.com/login");
    }

    @When("^pulsa_ en ya tengo cuenta$")
    public void pulsar_ya_tengo_cuenta() throws Exception {
        System.out.println("pulsamos en ya tengo cuenta");

        WebElement tengoCuentaBoton = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/p[1]/a"));
        tengoCuentaBoton.click();
    }

    @When("^introduce_ usuario y contraseña correctamente$")
    public void introduce_datos_correctos() throws Exception {
        System.out.println("Fill_ login form");

        WebElement userInput = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/form/input[1]"));
        WebElement passInput = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/form/input[2]"));
        userInput.clear();
        userInput.sendKeys("gerencia@maltessa.es");

        passInput.clear();
        passInput.sendKeys("Admin123");

        WebElement searchButton = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/form/button"));
        Thread.sleep(2000);
        searchButton.click();
        Thread.sleep(5000);
    }

    @When("^acepta_ el popup sobre privacidad$")
    public void acepta_privacidad() throws Exception {
        System.out.println("Aceptamos popup sobre privacidad");


        WebElement aceptaPrivacidadBoton = driver.findElement(By.xpath("//*[@id=\"didomi-notice-agree-button\"]/span"));
        Wait<WebDriver> fwait = new FluentWait<WebDriver>(driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(2, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
        aceptaPrivacidadBoton.click();

    }

    @Then("^el usuario_ puede ver su cuenta de usuario en biwenger$")
    public void user_sees_profile() throws Exception {
        System.out.println("User sees profile");

    }

    @When("^el usuario va desde su home de usuario a su equipo")
    public void ir_del_home_a_equipo() throws Exception {
        System.out.println("Vamos desde el home de usuario a la página de equipo");


        WebElement equipoBoton = driver.findElement(By.xpath("//*[@id=\"nav-team\"]"));
        Thread.sleep(2000);
        equipoBoton.click();
    }
}
/**
    @When("pulsa en vender en el jugador que quiera")
    public void pulsa_en_vender_en_el_jugador_que_quiera() throws Exception{


        driver.switchTo().frame("__tcfapiLocator");
        driver.findElement(By.xpath("/html/body/app-root/main/div/ng-component/tabs/div/tab[1]/div/div/user-squad/player-list/adv-list/div[1]/div[4]/player-card/div[2]/div[4]/market-tools/button"));

        //xpath("/html/body/app-root/main/div/ng-component/tabs/div/tab[1]/div/div/user-squad/player-list/adv-list/div[1]/div[4]/player-card/div[2]/div[4]/market-tools/button"));
        //venderBoton.click();


    }


    @When("pulsa en el precio máximo de clausula")
    public void pulsa_en_el_precio_máximo_de_clausula() {

        throw new io.cucumber.java.PendingException();
    }
    @When("pulsa en vender")
    public void pulsa_en_vender() {

        throw new io.cucumber.java.PendingException();
    }
    @Then("el jugador queda puesto a la venta y recibe un mensaje de confirmacion")
    public void el_jugador_queda_puesto_a_la_venta_y_recibe_un_mensaje_de_confirmacion() {

        throw new io.cucumber.java.PendingException();
    }





}
*/