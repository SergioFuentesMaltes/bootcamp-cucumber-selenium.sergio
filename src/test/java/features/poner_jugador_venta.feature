@runThis
Feature: Poner un jugador a la venta por su precio de clausula
  El usuario de Biwenger
  quiere poner un jugador a la venta
  para ofrecerlo por el precio de la clausula


  Background:
    Given el usuario_ accede a la web de biwenger del login
    When pulsa_ en ya tengo cuenta
    And introduce_ usuario y contraseña correctamente
    And acepta_ el popup sobre privacidad

    @JugadorVenta
    Scenario: El usuario pone al jugador a la venta por su precio de clausula correctamente
      When el usuario va desde su home de usuario a su equipo

     # And pulsa en vender en el jugador que quiera
     # And pulsa en el precio máximo de clausula
     # And pulsa en vender
     # Then el jugador queda puesto a la venta y recibe un mensaje de confirmacion